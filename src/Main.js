import React , {useState , useEffect} from 'react'
import { category } from './data/category'
import { product } from './data/product'
import { useDispatch, useSelector } from 'react-redux'
import {AddOrder} from './Redux/Action/Order'
import { useNavigate } from 'react-router-dom'




const Main = () => {
const [parentId, setParentId] = useState(1) //product
const [id, setId] = useState(1) //category
const [parentList, setParentList] = useState([]) //storing category data
const [list, setList] = useState([]) //storing product data
const [item , setItem] = useState([]) //category id === parent id => data store kerne ke liye
const [itemData, setItemData] = useState({}) //passing object multiple data we need to pass
const [itShow, setitShow] = useState(false) 
const [order , setOrder] = useState([]) //storing the choosed data
const [count , setCount] = useState(0)
const navigate = useNavigate()




// useEffect(() => {
//   const parentList = category.filter(data => data.parent === parentId )
//   setParentList(parentList)
//   setId(parentList[0].id) 
//   const List = product.filter(data => data.ParentId === parentList[0].id )
//   setList(List)
// }, [])

useEffect(() => { 
  const parentList = category.filter(data => data.parent === parentId )
  setParentList(parentList)
  setId(parentList[0].id) 
  const List = product.filter(data => data.ParentId === parentList[0].id )
  setList(List)
}, [parentId])

useEffect(() => {
  const item = product.filter(data => id === data.parentId)
  setItem(item)
}, [id])

const dispatch = useDispatch() //using disopatch we can change the state in redux

function handleOrder(item){

  console.log('itemdataaa',item)
  dispatch(AddOrder(item))
}


const myOrder = useSelector((state)=> state.order) // Useselcetor we use to get the data
console.log(myOrder);


function handleViewBasket(){
  navigate("/basket")
}
  // console.log(order);

  // function handleBasket(item){
  //   {order.map((item)=>{
  //     {item}
  //  })}
  // }

  return (
  
    <>
    <div className='first'>
        <div className='heading2'>
            <h2>Kings Arms Cardington</h2>
        </div>    
              <div className='para'>
                <p>134 High Street,Kempston,Bedford,</p>
                <p>Bedfordshire,MK42 7BN</p>
              </div>           
        
        <div className='head'>
            <div>
              <button  onClick={() => setParentId(1)} className={`btn ${parentId === 1 && 'yello-Back'}`}>DRINKS</button>
            </div>
            <div>
              <button onClick={() => setParentId(2)} className={`btn ${parentId === 2 && 'yello-Back'}`}>FOODS</button>
            </div>
            <div>
              <button  onClick={() => setParentId(3)}className={`btn ${parentId === 3 && 'yello-Back'}`}>SNACKS</button>
            </div>   
        </div>
        <br/>
        <ul className='starter'>
          
        {
          parentList.map(category => {
            return(
              
              <h3 key={category.id} onClick={() => {
                setitShow(false)
                setId(category.id)}}className={`stn ${id === category.id && 'yello1'}`}>
                {category.name}
              </h3>
            )
          }
          )}
        </ul>
        <div className='starter'>
          
        {
          list.map(category => {
            return(
              
              <div key={category.id} onClick={() => {
                setitShow(false)
                setId(category.id)}}className={`stn ${id === 1 && 'yello1'}`}>
                {category.name}
              </div>
            )
          }
          )}
        </div>
        <div className='insider'>

        {
          item.map(product => {
            return (

              <div key={product.id} onClick={() => {
                setId(product.parentId)
                setItemData(product)
                setitShow(true)
                setOrder({
                  id:product.id,
                  parentId: product.parentId,
                  name: product.name,
                  description:  product.description,
                  price: product.price,
                })
              }
             } >
              
               <div className='item1'>
                 <div className='top'>
                <h3>{product.name}</h3>
                <p>{product.description}</p>
                </div>
                <div className='price'>
                £{product.price}
                </div>
                </div>
              </div>
            )
          }
          )}
      </div>
    
      
    
      
    </div>    
       <div>
         {
            itShow && (
           <div>
            <h1>{itemData.name}</h1>
            <p>Lager {itemData.price}%</p>
            <h1>Size</h1>
            <ul>
              {itemData.variants.map((item)=>{
                  return (
                    <>
                    <input type = "radio" name="variant" onChange={()=>{
                      setOrder({...order,variants:item})
                    }}></input>
                      {item.name}
                      
                      {item.price}<br/>
                      
                    </>
                  )
              })}
            </ul>
            <h1>Select Options</h1>
            <ul>
              {itemData.extras.map((item)=> {
                return(
                  <>
                   <label>
                 <input type="checkbox"  onChange={()=>{
                   let array = order.extras || []
                   const isAdded =order.extras?.length>0 && order.extras.find((element) => element.name === item.name)
                   console.log(isAdded)
                   if(isAdded?.name){
                    array = order.extras.length>0 && order.extras.filter((element) => {
                     return element.name !== item.name
                     })
                    setOrder({...order,extras: array})
                   } else {
                     console.log(array)
                      // array.push(item)
                     setOrder({...order,extras:[...array,item]})
                   }
                    }}/>
                 {item.name}
                   (+ {item.price} )
                   </label> <br/>
                  </>
                )
              })}
            </ul>
            </div>
            )
        }
       </div> 

       <div>
         <button onClick={()=> setCount(count+1)}>+</button>
         {count}
         <button onClick={()=> {
           if(count>0)
           return setCount(count-1)}}>-</button>
       </div>
       <div>
         <button onClick={()=> handleOrder(order)}>ADD TO ORDER</button>
       </div>
       
       <div>
         <button onClick={handleViewBasket}>View Basket</button>
         <h1></h1>
       </div>
       </>
   
  )
}
export default Main